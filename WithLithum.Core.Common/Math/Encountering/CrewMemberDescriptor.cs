namespace WithLithum.Core.Math.Encountering;

/// <summary>
/// Represents a generic purpose crew member or protagonist.
/// This class may be extended to provide game-specific features.
/// </summary>
public class CrewMemberDescriptor
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CrewMemberDescriptor"/> class.
    /// </summary>
    /// <param name="level">The level.</param>
    /// <param name="defaultHealth">The maximum health.</param>
    public CrewMemberDescriptor(int level, int defaultHealth)
    {
        Level = level;
        Health = defaultHealth;
        MaxHealth = defaultHealth;
    }

    /// <summary>
    /// Gets or sets the level of this instance.
    /// </summary>
    /// <value>An <see cref="int" /> describing the level of this instance.</value>
    public int Level { get; set; }

    /// <summary>
    /// Gets or sets the health of this instance.
    /// </summary>
    /// <value>An <see cref="int" /> describing the health of this instance.</value>
    public int Health { get; set; }

    /// <summary>
    /// Gets or sets the maximum health of this instance.
    /// </summary>
    /// <value>The maximum health of this instance.</value>
    public int MaxHealth { get; set; }
}
