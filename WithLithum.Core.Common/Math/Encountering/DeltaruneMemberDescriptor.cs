namespace WithLithum.Core.Math.Encountering;

/// <summary>
/// Represents a generic purpose Deltarune member descriptor.
/// </summary>
public class DeltaruneMemberDescriptor : CrewMemberDescriptor
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DeltaruneMemberDescriptor"/> class.
    /// </summary>
    /// <param name="level">The level.</param>
    /// <param name="maxHealth">The maximum health.</param>
    public DeltaruneMemberDescriptor(int level, int maxHealth) : base(level, maxHealth)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DeltaruneMemberDescriptor"/> class.
    /// </summary>
    /// <param name="level">The level.</param>
    /// <param name="maxHealth">The maximum health.</param>
    /// <param name="at">The AT (attack) attribute.</param>
    /// <param name="df">The DF (defense) attribute.</param>
    /// <param name="magic">The Magic attribute.</param>
    public DeltaruneMemberDescriptor(int level, int maxHealth, int at, int df, int magic)
        :this(level, maxHealth)
    {
        Attack = at;
        Defense = df;
        Magic = magic;
    }

    /// <summary>
    /// Gets or sets the attack (AT) attribute of this instance, affecting the
    /// amount of damage this instance inflicts.
    /// </summary>
    /// <value>An <see cref="int"/> representing the attack attribute of this instance.</value>
    public int Attack { get; set; }

    /// <summary>
    /// Gets or sets the defense (DF) attribute of this instance, affecting the
    /// amount of damage this instance takes.
    /// </summary>
    /// <value>An <see cref="int"/> representing the defense attribute of this instance.</value>
    public int Defense { get; set; }

    /// <summary>
    /// Gets or sets the Magic attribute of this instance, affecting various
    /// spells.
    /// </summary>
    /// <value>An <see cref="int"/> representing the magic attribute of this instance.</value>
    public int Magic { get; set; }
}