namespace WithLithum.Core.Math.Encountering.Simulation;

/// <summary>
/// Represents the values regarding a violently ended encounter.
/// </summary>
public class ViolentWinAttributes
{
    /// <summary>
    /// Gets or sets the increment of <see cref="DeltaruneMemberDescriptor.Attack"/>
    /// for each encounter ended violently.
    /// </summary>
    /// <value>An <see cref="int"/> representing the increment.</value>
    public int AttackIncrement { get; set; }
}