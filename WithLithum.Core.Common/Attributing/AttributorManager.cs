﻿namespace WithLithum.Core.Attributing;
using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Provides support of attributors.
/// </summary>
public static class AttributorManager
{
    private static readonly Dictionary<Type, Type> _attributors = new();

    /// <summary>
    /// Acquires an existing attributor.
    /// </summary>
    /// <typeparam name="T">The type of the class to attribute.</typeparam>
    /// <typeparam name="TOutput">The output attributor type.</typeparam>
    /// <param name="obj"></param>
    /// <returns>The instance.</returns>
    /// <exception cref="NotSupportedException">No attributor for the specified type exists.</exception>
    public static TOutput Attributor<T, TOutput>(this T obj) where TOutput : IAttributor<T>, new()
    {
        if (!_attributors.ContainsKey(typeof(T)))
        {
            throw new NotSupportedException("The type provided does not have an attributor at the time of calling.");
        }

        var t = _attributors[typeof(T)];
        if (t == null)
        {
            throw new NotSupportedException("The type provided does not have a valid attributor at the time of calling.");
        }

        var instance = Activator.CreateInstance<TOutput>();
        instance.Apply(obj);
        return instance;
    }

    /// <summary>
    /// Registers an attributor for the specified target.
    /// </summary>
    /// <typeparam name="TTarget">The target.</typeparam>
    /// <typeparam name="TAttributor">The attributor.</typeparam>
    /// <exception cref="InvalidOperationException">An attributor already exists for <typeparamref name="TTarget"/>.</exception>
    public static void RegisterAttributor<TTarget, TAttributor>() where TAttributor : IAttributor<TTarget>, new()
    {
        if (_attributors.ContainsKey(typeof(TTarget)))
        {
            throw new InvalidOperationException("Already existing attributor registered for the object you provided.");
        }

        _attributors.Add(typeof(TTarget), typeof(TAttributor));
    }
}
