﻿namespace WithLithum.Core.Attributing;

using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Represents an object that attributes another object.
/// </summary>
/// <typeparam name="T">The type to attribute.</typeparam>
public interface IAttributor<in T>
{
    /// <summary>
    /// Accepts the specified object.
    /// </summary>
    /// <param name="value">The object to accept.</param>
    void Apply(T value);
}
