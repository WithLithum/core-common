﻿using System.Diagnostics;

namespace WithLithum.Core.Diagnostics;

/// <summary>
/// Provides methods related to <see cref="Process"/>es.
/// </summary>
public static class ProcessUtil
{
    /// <summary>
    /// Executes the specified file via the shell of the operating system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// In .NET Core and .NET 5+, the <see cref="ProcessStartInfo.UseShellExecute"/> was set to <see langword="false"/> by default,
    /// and shell-executing requires the programmer to explicitly create a <see cref="ProcessStartInfo"/> themselves and
    /// set the property to true and then use <see cref="Process.Start(ProcessStartInfo)"/> with the info instance they created
    /// in order to open a file with it's associated program of it's type.
    /// </para>
    /// <para>
    /// This method creates a <see cref="ProcessStartInfo"/> internally, set <see cref="ProcessStartInfo.UseShellExecute"/> to <see langword="true"/>,
    /// and then starts with the info, so it behaves the same like <see cref="Process.Start(string)"/> in .NET Framework.
    /// </para>
    /// <note type="note">
    /// If you are on .NET Framework, you do not have to call this method. This method is only intended for non-Framework users.
    /// </note>
    /// </remarks>
    /// <example>
    /// <para>This example opens a text file called <c>WhetherItIs.txt</c> via the associated text file editor of the operating system.</para>
    /// <code language="cs">
    /// // This code assumes you use C# 10 or you manually add required structure to it.
    ///
    /// using System;
    /// using WithLithum.Core.Diagnostics;
    ///
    /// ProcessUtil.ShellExecute("WhetherItIs.txt");
    /// </code>
    /// </example>
    /// <param name="path">The path to the file.</param>
    /// <seealso cref="Process"/>
    public static Process ShellExecute(string path)
    {
        var info = new ProcessStartInfo(path)
        {
            UseShellExecute = true
        };

        return Process.Start(info);
    }
}
