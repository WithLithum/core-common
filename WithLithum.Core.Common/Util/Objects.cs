﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WithLithum.Core.Util;

/// <summary>
/// Provides utilities to for operating on objects.
/// </summary>
/// <remarks>
/// This class is an implementation of <a href="https://docs.oracle.com/javase/8/docs/api/java/util/Objects.html">Objects</a> class
/// of the Java Standard framework.
/// </remarks>
public static class Objects
{
    /// <summary>
    /// Determines whether the object <paramref name="a"/> is equivalent to <paramref name="b"/>.
    /// </summary>
    /// <param name="a">The first object.</param>
    /// <param name="b">The second object.</param>
    /// <returns><c>true</c> if the first object equals to second object; otherwise, <c>false</c>.</returns>
    public static new bool Equals(object a, object b)
    {
        return a?.Equals(b) == true;
    }

    /// <summary>
    /// Gets the hash code of the specified instance.
    /// </summary>
    /// <param name="obj">The name of the object.</param>
    /// <returns>If <paramref name="obj"/> exists, return the specified hashcode; otherwise, <c>0</c>.</returns>
    public static int HashCode(object obj)
    {
        return (obj?.GetHashCode()) ?? 0;
    }

    /// <summary>
    /// Determines whether the specified instance is null.
    /// </summary>
    /// <param name="obj">The object to check.</param>
    /// <returns><c>true</c> if the specified object is <see langword="null"/>; otherwise, <c>false</c>.</returns>
    public static bool IsNull(object? obj) => obj == null;

    /// <summary>
    /// Determines whether the specified instance is not null.
    /// </summary>
    /// <param name="obj">The object to check.</param>
    /// <returns><c>true</c> if the specified object is not <see langword="null"/>; otherwise, <c>false</c>.</returns>
    public static bool NonNull(object? obj) => !IsNull(obj);

    /// <summary>
    /// Checks whether the specified object is null, and if it is, throws an exception.
    /// </summary>
    /// <typeparam name="T">The type of the object.</typeparam>
    /// <param name="obj">The object.</param>
    /// <exception cref="NullReferenceException">Thrown if <paramref name="obj"/> is null.</exception>
    public static T RequireNonNull<T>(T obj)
    {
        if (IsNull(obj)) throw new NullReferenceException();
        return obj;
    }

    /// <summary>
    /// Checks whether the specified object is null, and if it is, throws an exception with the specified <paramref name="message"/>.
    /// </summary>
    /// <typeparam name="T">The type of the object.</typeparam>
    /// <param name="obj">The object.</param>
    /// <param name="message">The message of the excepton,</param>
    /// <exception cref="NullReferenceException">Thrown if <paramref name="obj"/> is null.</exception>
    public static T RequireNonNull<T>(T obj, string message)
    {
        if (IsNull(obj)) throw new NullReferenceException(message);
        return obj;
    }

    /// <summary>
    /// Checks whether the specified argument is null, and if it is, throws an exception.
    /// </summary>
    /// <typeparam name="T">The type of the object.</typeparam>
    /// <param name="obj">The object.</param>
    /// <param name="argName">The name of the argument.</param>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="obj"/> is null.</exception>
    public static T RequireNonNullArg<T>(T obj, string argName)
    {
        if (IsNull(obj)) throw new ArgumentNullException(argName);
        return obj;
    }

    /// <summary>
    /// Checks whether the specified argument is null, and if it is, throws an exception.
    /// </summary>
    /// <typeparam name="T">The type of the object.</typeparam>
    /// <param name="obj">The object.</param>
    /// <param name="argName">The name of the argument.</param>
    /// <param name="message">The message.</param>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="obj"/> is null.</exception>
    public static T RequireNonNullArg<T>(T obj, string argName, string message)
    {
        if (IsNull(obj)) throw new ArgumentNullException(argName, message);
        return obj;
    }

    /// <summary>
    /// Checks whether the specified object is null, and if it is, throws an exception.
    /// </summary>
    /// <typeparam name="T">The type of the object.</typeparam>
    /// <typeparam name="TEx">The type of the exception.</typeparam>
    /// <param name="obj">The object.</param>
    /// <exception cref="NullReferenceException">Thrown if <paramref name="obj"/> is null.</exception>
    public static T RequireNonNull<T, TEx>(T obj) where TEx : Exception, new()
    {
        if (IsNull(obj)) throw new TEx();
        return obj;
    }

    /// <summary>
    /// Converts the specified object to string.
    /// </summary>
    /// <param name="obj">The object.</param>
    /// <returns>If <paramref name="obj"/> was null, return null; otherwise, the string representation of the obj.</returns>       
    public static string? ToString(object? obj)
    {
        return obj?.ToString();
    }

    /// <summary>
    /// Converts the specified object to string.
    /// </summary>
    /// <param name="obj">The object.</param>
    /// <param name="nullDefault">The default value if <paramref name="obj"/> was null.</param>
    /// <returns>If <paramref name="obj"/> was null, return <paramref name="nullDefault"/>; otherwise, the string representation of the obj.</returns>       
    public static string ToString(object? obj, string nullDefault)
    {
        var result = ToString(obj);
        return result ?? nullDefault;
    }
}
