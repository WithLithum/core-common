﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WithLithum.Core.Util;

/// <summary>
/// Represents an object that has a handle.
/// </summary>
/// <typeparam name="HandleType">The type of the handle.</typeparam>
public interface IHandleable<HandleType>
{
    /// <summary>
    /// Gets the handle of this instance.
    /// </summary>
    HandleType Handle { get; }
}
