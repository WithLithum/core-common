﻿// Copyright (C) WithLithum 2021-2022
// Licensed under Apache-2.0 license

namespace WithLithum.Core.ConsoleUtil;

using System;

/// <summary>
/// Provide utilities to ask user questions in a console window.
/// </summary>
public static class Question
{
    /// <summary>
    /// Asks a question which user answers yes or no.
    /// </summary>
    /// <param name="question">The question summary.</param>
    /// <returns><c>true</c> if user answers yes; otherwise, <c>false</c>.</returns>
    public static bool AskBooleanQuestion(string question)
    {
        Console.Write(question + " (y/[n/other])");
        var read = Console.ReadKey();
        Console.WriteLine();
        return read.Key == ConsoleKey.Y;
    }

    /// <summary>
    /// Asks a question which user answers a <see cref="string"/>.
    /// </summary>
    /// <param name="question">The question summary.</param>
    /// <returns>The <see cref="string"/> that the user answers.</returns>
    public static string? AskStringQuestion(string question)
    {
        Console.Write(question);
        return Console.ReadLine();
    }
}
