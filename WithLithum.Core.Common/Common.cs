﻿using System;

namespace WithLithum.Core;

/// <summary>
/// Provide Common Utilties.
/// </summary>
public static class Common
{
    /// <summary>
    /// Generates a random integer.
    /// </summary>
    /// <param name="min">The minimum value of the random integer, inclusive.</param>
    /// <param name="max">The maximum value of the random integer, exclusive.</param>
    /// <returns>The generated random integer.</returns>
    public static int GetRandomInteger(int min, int max)
    {
        return new Random().Next(min, max);
    }

    /// <summary>
    /// Generates a random integer.
    /// </summary>
    /// <param name="max">The maximum value of the random integer, inclusive.</param>
    /// <returns>The generated random integer.</returns>
    public static int GetRandomInteger(int max)
    {
        return new Random().Next(max + 1);
    }
}
