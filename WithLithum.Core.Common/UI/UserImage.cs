﻿namespace WithLithum.Core.UI;

using System;
using Util;

/// <summary>
/// Represents an image.
/// </summary>
public class UserImage : IHandleable<IntPtr>
{
    /// <summary>
    /// The type of the image.
    /// </summary>
    public enum ImageType
    {
        /// <summary>
        /// Loads the file as a bitmap file.
        /// </summary>
        Bitmap,
        /// <summary>
        /// An icon file.
        /// </summary>
        Icon,
        /// <summary>
        /// A cursor file.
        /// </summary>
        Cursor
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="UserImage"/> class.
    /// </summary>
    /// <param name="fileName">The file to load.</param>
    /// <param name="type">The type.</param>
    public UserImage(string fileName, ImageType type)
    {
        Handle = UserFunctions.LoadImage(IntPtr.Zero, fileName, (uint)type, 0, 0, (uint)UserFunctions.LoadImageFlags.LR_LOADFROMFILE);
    }

    /// <summary>
    /// Gets the handle of this instance.
    /// </summary>
    public IntPtr Handle { get; }
}
