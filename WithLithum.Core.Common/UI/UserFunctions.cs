﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WithLithum.Core.UI;

/// <summary>
/// Provide static methods to manipulate Win32 related stuff.
/// </summary>
public static class UserFunctions
{
    [Flags]
    internal enum LoadLibraryFlags : uint
    {
        DONT_RESOLVE_DLL_REFERENCES = 0x00000001,
        LOAD_LIBRARY_AS_DATAFILE = 0x00000002,
        LOAD_WITH_ALTERED_SEARCH_PATH = 0x00000008,
        LOAD_IGNORE_CODE_AUTHZ_LEVEL = 0x00000010,
        LOAD_LIBRARY_AS_IMAGE_RESOURCE = 0x00000020,
        LOAD_LIBRARY_AS_DATAFILE_EXCLUSIVE = 0x00000040
    }

    [Flags]
    internal enum LoadImageFlags : uint
    {
        LR_DEFAULTCOLOR = 0x00000000,
        LR_LOADFROMFILE = 0x00000010,
        LR_DEFAULTSIZE = 0x00000040,
        LR_CREATEDIBSECTION = 0x00002000,
    }

    [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    internal static extern IntPtr LoadImage(IntPtr hinst, string lpszName, uint uType, int cxDesired, int cyDesired, uint fuLoad);
}
