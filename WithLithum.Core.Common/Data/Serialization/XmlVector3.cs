﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace WithLithum.Core.Data.Serialization;

/// <summary>
/// Represents a three-dimensional vector in XML.
/// </summary>
public struct XmlVector3 : IEquatable<XmlVector3>
{
    /// <summary>
    /// Gets or sets the X asis value of this dimensional vector.
    /// </summary>
    [XmlAttribute("x")]
    public float X { get; set; }

    /// <summary>
    /// Gets or sets the Y asis value of this dimensional vector.
    /// </summary>
    [XmlAttribute("y")]
    public float Y { get; set; }

    /// <summary>
    /// Gets or sets the Z asis value of this dimensional vector.
    /// </summary>
    [XmlAttribute("z")]
    public float Z { get; set; }

    /// <inheritdoc />
    public override bool Equals(object obj)
    {
        if (!(obj is XmlVector3 vector)) return false;
        else return Equals(vector);
    }

    /// <inheritdoc />
    public bool Equals(XmlVector3 other)
    {
        return (other.X == this.X) && (other.Y == this.Y) && (other.Z == this.Z);
    }

    /// <summary>
    /// Determines whether the value on the <paramref name="left"/> is equivalent to the value on the <paramref name="right"/>.
    /// </summary>
    /// <param name="left">The value on the left side of this operator.</param>
    /// <param name="right">The value of the right side of this operator.</param>
    /// <returns>If <paramref name="left"/> is equivalent to <paramref name="right"/>, <see langword="true"/>; otherwise, <see langword="false"/>.</returns>
    public static bool operator ==(XmlVector3 left, XmlVector3 right)
    {
        return left.Equals(right);
    }

    /// <summary>
    /// Determines whether the value on the <paramref name="left"/> isn't equivalent to the value on the <paramref name="right"/>.
    /// </summary>
    /// <param name="left">The value on the left side of this operator.</param>
    /// <param name="right">The value of the right side of this operator.</param>
    /// <returns>If <paramref name="left"/> is not equivalent to <paramref name="right"/>, <see langword="true"/>; otherwise, <see langword="false"/>.</returns>
    public static bool operator !=(XmlVector3 left, XmlVector3 right)
    {
        return !(left == right);
    }

#pragma warning disable RCS1132 // Remove redundant overriding member.

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
#pragma warning restore RCS1132 // Remove redundant overriding member.
}