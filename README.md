# WithLithum Core: Common

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/withlithum/core-common?logo=gitlab) ![GitLab tag (latest by date)](https://img.shields.io/gitlab/v/tag/withlithum/core-common)

This library is a common library for WithLithum codes across several CLI implementations, such as .NET Framework, .NET Core / .NET 5+, and Mono, etc., for different purposes, such as:

* Generic applications
* Windows Desktop (WPF, Forms / WinUI) applications
* Linux Desktop (GTK# & Glade) applications
* GTA modifications
* Probably games

## How to build & contribute

Just like how you build a regular .NET project. If you don't know how there is a tutorial in the [contributing guide](CONTRIBUTING.md).

Also you may refer to Contributing Guide for how to contribute and some basic requirements.
