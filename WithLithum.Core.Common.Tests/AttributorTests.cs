namespace WithLithum.Core.Tests;

using System;
using WithLithum.Core.Attributing;
using Xunit;

public class AttributorTests
{
    public class AttributorTestType
    {
        public string Bal { get; set; } = "";
    }

    public class TestAttributor : IAttributor<AttributorTestType>
    {
        private AttributorTestType? _type;

        public static void RegisterMe()
        {
            AttributorManager.RegisterAttributor<AttributorTestType, TestAttributor>();
        }

        public void Apply(AttributorTestType value)
        {
            _type = value;
        }

        public void SetBal()
        {
            if (_type == null)
            {
                throw new NullReferenceException("Invalid attributor base type.");
            }

            _type.Bal = "BAL";
        }
    }

    /// <summary>
    /// Tests the registration and finding for the attributors.
    /// </summary>
    [Fact]
    public void AttributorFinding()
    {
        try
        {
            TestAttributor.RegisterMe();
        }
        catch (InvalidOperationException)
        { 
            // In case of already registered.
        }

        var t = new AttributorTestType();
        var att = t.Attributor<AttributorTestType, TestAttributor>();

        Assert.NotNull(att);
    }

    [Fact]
    public void AttributorOperation()
    {
        try
        {
            TestAttributor.RegisterMe();
        }
        catch (InvalidOperationException)
        {
            // In case of already registered.
        }

        var t = new AttributorTestType();
        var att = t.Attributor<AttributorTestType, TestAttributor>();

        Assert.NotNull(att);
        att?.SetBal();

        Assert.True(t.Bal == "BAL");
    }
}